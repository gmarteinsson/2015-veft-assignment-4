﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using CoursesAPI.Models;
using CoursesAPI.Services.Exceptions;
using CoursesAPI.Services.Models.Entities;
using CoursesAPI.Services.Services;
using CoursesAPI.Tests.MockObjects;
using CoursesAPI.Tests.TestExtensions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CoursesAPI.Tests.Services
{
	[TestClass]
	public class CourseServicesTests
	{
		private MockUnitOfWork<MockDataContext> _mockUnitOfWork;
		private CoursesServiceProvider _service;
		private List<TeacherRegistration> _teacherRegistrations;
        
        private const string SSN_DABS    = "1203735289";
		private const string SSN_GUNNA   = "1234567890";
		private const string INVALID_SSN = "9876543210";

		private const string NAME_GUNNA  = "Guðrún Guðmundsdóttir";

		private const int COURSEID_VEFT_20153 = 1337;
		private const int COURSEID_VEFT_20163 = 1338;
        private const int COURSEID_VEFT_20162 = 1339;
        private const int INVALID_COURSEID    = 9999;

        const string SEMSESTER = "20153";
        const string SEMESTER_NEXT = "20163";
        const string SEMESTER_NEW = "20162";
        const string COURSEID = "T-514-VEFT";
        const string COURSEID_NEW = "T-409-TSAM";
        const string COURSENAME = "Vefþjónustur";
        const string COURSENAME_NEW = "Tölvusamskipti";
        //const int COURSEINSTANCEID = 1;


        [TestInitialize]
		public void Setup()
		{
			

			#region Persons
			var persons = new List<Person>
			{
				// Of course I'm the first person,
				// did you expect anything else?
				new Person
				{
					ID    = 1,
					Name  = "Daníel B. Sigurgeirsson",
					SSN   = SSN_DABS,
					Email = "dabs@ru.is"
				},
				new Person
				{
					ID    = 2,
					Name  = NAME_GUNNA,
					SSN   = SSN_GUNNA,
					Email = "gunna@ru.is"
				},
                new Person
                {
                    ID    = 3,
                    Name  = "Sigurður M. Atlason",
                    SSN   = "2011922189",
                    Email = "sigurdura13@ru.is"
                }
			};
			#endregion

			#region Course templates

			var courseTemplates = new List<CourseTemplate>
			{
				new CourseTemplate
				{
					CourseID    = COURSEID,
					Description = "Í þessum áfanga verður fjallað um vefþj...",
					Name        = COURSENAME
                },
                new CourseTemplate
                {
                    CourseID    = COURSEID_NEW,
                    Description = "Í þessum áfanga verður fjallað um tölvusamskipti...",
                    Name        = COURSENAME_NEW
                }
			};
			#endregion

			#region Courses
			var courseInstances = new List<CourseInstance>
			{
				new CourseInstance
				{
					ID         = COURSEID_VEFT_20153,
					CourseID   = COURSEID,
					SemesterID = SEMSESTER
                },
				new CourseInstance
				{
					ID         = COURSEID_VEFT_20163,
					CourseID   = COURSEID,
					SemesterID = SEMESTER_NEXT
                },
                new CourseInstance
                {
                    ID          = COURSEID_VEFT_20162,
                    CourseID    = COURSEID_NEW,
                    SemesterID  = SEMESTER_NEW
                }
			};
			#endregion

			#region Teacher registrations
			_teacherRegistrations = new List<TeacherRegistration>
			{
				new TeacherRegistration
				{
					ID               = 101,
					CourseInstanceID = COURSEID_VEFT_20153,
					SSN              = SSN_DABS,
					Type             = TeacherType.MainTeacher
				}
			};
			#endregion

			_mockUnitOfWork = new MockUnitOfWork<MockDataContext>();
            _mockUnitOfWork.SetRepositoryData(persons);
			_mockUnitOfWork.SetRepositoryData(courseTemplates);
			_mockUnitOfWork.SetRepositoryData(courseInstances);
			_mockUnitOfWork.SetRepositoryData(_teacherRegistrations);

            // TODO: this would be the correct place to add 
            // more mock data to the mockUnitOfWork!

            _service = new CoursesServiceProvider(_mockUnitOfWork);
		}

        #region GetCoursesBySemester

        /// <summary>
        /// Tests if GetCourseInstancesBySemester returns all courses in 20153 with 
        /// parameter passed in.
        /// </summary>
        [TestMethod]
        public void GetCourseBySemesterWithData()
        {
            // Act:
            var result = _service.GetCourseInstancesBySemester(SEMSESTER);

            // Assert:
            // Based on test data declared in Setup(),
            // this should be the result:
            Assert.AreEqual(1, result.Count, "The number of courses is correct");
            var courseDTO = result[0];

            Assert.AreEqual(COURSENAME, courseDTO.Name);
            Assert.AreEqual(COURSEID, courseDTO.TemplateID);
            Assert.AreEqual(COURSEID_VEFT_20153, courseDTO.CourseInstanceID);
        }

        /// <summary>
        /// Tests if GetCourseInstancesBySemester returns all courses in 20153 if no 
        /// parameter is passed in.
        /// </summary>
        [TestMethod]
	    public void GetCourseBySemesterWithEmptySemesterParameter()
	    {
            // Act:
            var result = _service.GetCourseInstancesBySemester();

            // Assert:
            Assert.AreEqual(1,result.Count, "The number of courses is correct");
            var courseDTO = result[0];

            Assert.AreEqual(COURSEID, courseDTO.TemplateID);
            Assert.AreEqual(COURSENAME, courseDTO.Name);
            Assert.AreEqual(COURSEID_VEFT_20153, courseDTO.CourseInstanceID);
	    }

        /// <summary>
        /// Asks for semsester with no course.
        /// </summary>
        [TestMethod]
        public void GetCourseBySemesterWithDataOnDifferentSemester()
        {
            // Arrange:
            const string SEMESTER_REQUESTED = "20151";
            // Act:
            var result = _service.GetCourseInstancesBySemester(SEMESTER_REQUESTED);

            // Assert:
            Assert.AreEqual(0, result.Count, "The number of course is incorrect.");
        }

        /// <summary>
        /// checks if a given semester has no course
        /// </summary>
	    [TestMethod]
	    public void GetCourseBySemesterWithNoCourses()
	    {
            // Act:
	        var result = _service.GetCourseInstancesBySemester("20151");

	        // Assert:
            Assert.AreEqual(0,result.Count, "The number of courses is incorrect");
	    }

        /// <summary>
        /// checks for the name of the main teacher of the course
        /// </summary>
        [TestMethod]
        public void GetCourseBySemesterWithNameOfMainTeacher()
        {
            // Act:
            var result = _service.GetCourseInstancesBySemester(SEMSESTER);
            // Assert:
            var courseDTO = result[0];

            Assert.AreEqual(courseDTO.Name, COURSENAME);
            Assert.AreEqual(COURSEID_VEFT_20153, courseDTO.CourseInstanceID);
            Assert.AreEqual("Daníel B. Sigurgeirsson", courseDTO.MainTeacher);
            //Assert.AreEqual();

        }

        /// <summary>
        /// Checks if main teacher in course is not registered for the course yet
        /// if its empty then it returns empty string
        /// </summary>
        [TestMethod]
        public void GetCourseBySemesterWithEmptyNameOfMainTeacher()
        {
            // Act:
            var result = _service.GetCourseInstancesBySemester(SEMESTER_NEW);
            // Assert:
            var courseDTO = result[0];

            Assert.AreEqual(courseDTO.Name, COURSENAME_NEW);
            Assert.AreEqual(COURSEID_VEFT_20162, courseDTO.CourseInstanceID);
            Assert.AreEqual("", courseDTO.MainTeacher);
            //Assert.AreEqual();

        }

        #endregion

        #region AddTeacher

        /// <summary>
        /// Adds a main teacher to a course which doesn't have a
        /// main teacher defined already (see test data defined above).
        /// </summary>
        [TestMethod]
		public void AddTeacher_WithValidTeacherAndCourse()
		{
			// Arrange:
			var model = new AddTeacherViewModel
			{
				SSN  = SSN_GUNNA,
				Type = TeacherType.MainTeacher
			};
			var prevCount = _teacherRegistrations.Count;
			// Note: the method uses test data defined in [TestInitialize]

			// Act:
			var dto = _service.AddTeacherToCourse(COURSEID_VEFT_20163, model);

			// Assert:

			// Check that the dto object is correctly populated:
			Assert.AreEqual(SSN_GUNNA, dto.SSN);
			Assert.AreEqual(NAME_GUNNA, dto.Name);

			// Ensure that a new entity object has been created:
			var currentCount = _teacherRegistrations.Count;
			Assert.AreEqual(prevCount + 1, currentCount);

			// Get access to the entity object and assert that
			// the properties have been set:
			var newEntity = _teacherRegistrations.Last();
			Assert.AreEqual(COURSEID_VEFT_20163, newEntity.CourseInstanceID);
			Assert.AreEqual(SSN_GUNNA, newEntity.SSN);
			Assert.AreEqual(TeacherType.MainTeacher, newEntity.Type);

			// Ensure that the Unit Of Work object has been instructed
			// to save the new entity object:
			Assert.IsTrue(_mockUnitOfWork.GetSaveCallCount() > 0);
		}

		[TestMethod]
		[ExpectedException(typeof(AppObjectNotFoundException))]
		public void AddTeacher_InvalidCourse()
		{
			// Arrange:
			var model = new AddTeacherViewModel
			{
				SSN  = SSN_GUNNA,
				Type = TeacherType.AssistantTeacher
			};
			// Note: the method uses test data defined in [TestInitialize]

			// Act:
			_service.AddTeacherToCourse(INVALID_COURSEID, model);
		}

		/// <summary>
		/// Ensure it is not possible to add a person as a teacher
		/// when that person is not registered in the system.
		/// </summary>
		[TestMethod]
		[ExpectedException(typeof (AppObjectNotFoundException))]
		public void AddTeacher_InvalidTeacher()
		{
			// Arrange:
			var model = new AddTeacherViewModel
			{
				SSN  = INVALID_SSN,
				Type = TeacherType.MainTeacher
			};
			// Note: the method uses test data defined in [TestInitialize]

			// Act:
			_service.AddTeacherToCourse(COURSEID_VEFT_20163, model);
		}

		/// <summary>
		/// In this test, we test that it is not possible to
		/// add another main teacher to a course, if one is already
		/// defined.
		/// </summary>
		[TestMethod]
		[ExpectedExceptionWithMessage(typeof (AppValidationException), "COURSE_ALREADY_HAS_A_MAIN_TEACHER")]
		public void AddTeacher_AlreadyWithMainTeacher()
		{
			// Arrange:
			var model = new AddTeacherViewModel
			{
				SSN  = SSN_GUNNA,
				Type = TeacherType.MainTeacher
			};
			// Note: the method uses test data defined in [TestInitialize]

			// Act:
			_service.AddTeacherToCourse(COURSEID_VEFT_20153, model);
		}

		/// <summary>
		/// In this test, we ensure that a person cannot be added as a
		/// teacher in a course, if that person is already registered
		/// as a teacher in the given course.
		/// </summary>
		[TestMethod]
		[ExpectedExceptionWithMessage(typeof (AppValidationException), "PERSON_ALREADY_REGISTERED_TEACHER_IN_COURSE")]
		public void AddTeacher_PersonAlreadyRegisteredAsTeacherInCourse()
		{
			// Arrange:
			var model = new AddTeacherViewModel
			{
				SSN  = SSN_DABS,
				Type = TeacherType.AssistantTeacher
			};
			// Note: the method uses test data defined in [TestInitialize]

			// Act:
			_service.AddTeacherToCourse(COURSEID_VEFT_20153, model);
		}

		#endregion
	}
}
