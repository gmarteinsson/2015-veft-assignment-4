﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using CoursesAPI.Models;
using CoursesAPI.Services.DataAccess;
using CoursesAPI.Services.Exceptions;
using CoursesAPI.Services.Models.Entities;

namespace CoursesAPI.Services.Services
{
	public class CoursesServiceProvider
	{
		private readonly IUnitOfWork _uow;

		private readonly IRepository<CourseInstance> _courseInstances;
		private readonly IRepository<TeacherRegistration> _teacherRegistrations;
		private readonly IRepository<CourseTemplate> _courseTemplates; 
		private readonly IRepository<Person> _persons;

		public CoursesServiceProvider(IUnitOfWork uow)
		{
			_uow = uow;

			_courseInstances      = _uow.GetRepository<CourseInstance>();
			_courseTemplates      = _uow.GetRepository<CourseTemplate>();
			_teacherRegistrations = _uow.GetRepository<TeacherRegistration>();
			_persons              = _uow.GetRepository<Person>();
		}

		/// <summary>
		/// You should implement this function, such that all tests will pass.
		/// </summary>
		/// <param name="courseInstanceID">The ID of the course instance which the teacher will be registered to.</param>
		/// <param name="model">The data which indicates which person should be added as a teacher, and in what role.</param>
		/// <returns>Should return basic information about the person.</returns>
		public PersonDTO AddTeacherToCourse(int courseInstanceID, AddTeacherViewModel model)
		{
		    var person = _persons.All().SingleOrDefault(x => x.SSN == model.SSN);
		    var course = _courseInstances.All().SingleOrDefault(x => x.ID == courseInstanceID);
		    var teacherReg = _teacherRegistrations.All().SingleOrDefault(x => x.SSN == model.SSN);
		    //var isMainTeacher = _teacherRegistrations.All().SingleOrDefault(x => x.Type == TeacherType.MainTeacher);


		    var isTeacher = (from cr in _courseInstances.All()
		        join tr in _teacherRegistrations.All() on cr.ID equals tr.CourseInstanceID
		        where tr.CourseInstanceID == courseInstanceID
		        select tr.Type).SingleOrDefault();

            // Check if course is valid
		    if (course == null)
		    {
		        throw new AppObjectNotFoundException();
		    }

            // Check if person is valid
		    if (person == null)
		    {
		        throw new AppObjectNotFoundException();
		    }

            // Check if the person is already registred as a teacher in course
		    if (teacherReg != null)
		    {
		        throw new AppValidationException("PERSON_ALREADY_REGISTERED_TEACHER_IN_COURSE");
		    }

            // Course can only have one main teacher.
		    if (isTeacher == TeacherType.MainTeacher) 
		    {
		        throw new AppValidationException("COURSE_ALREADY_HAS_A_MAIN_TEACHER");
		    }

            // Adds the teacher
		    var addTeacher = new TeacherRegistration()
		    {
                SSN = model.SSN,
                CourseInstanceID = courseInstanceID,
                Type = model.Type
		    };
		    
            
            // Saves the data
            _teacherRegistrations.Add(addTeacher);
            _uow.Save();

            // Returns a simple info about the person
		    var returnValue = new PersonDTO
		    {
		        SSN = model.SSN,
		        Name = person.Name
		    };

            return returnValue;
		}

		/// <summary>
		/// You should write tests for this function. You will also need to
		/// modify it, such that it will correctly return the name of the main
		/// teacher of each course.
		/// </summary>
		/// <param name="semester"></param>
		/// <returns></returns>
		public List<CourseInstanceDTO> GetCourseInstancesBySemester(string semester = null)
		{
			if (string.IsNullOrEmpty(semester))
			{
				semester = "20153";
			}

		    var coursesLeftQuery = from c in _courseInstances.All()
		        join ct in _courseTemplates.All() on c.CourseID equals ct.CourseID
                where c.SemesterID == semester
		        select new {c, ct};

		    var teachersRightQuery = from tr in _teacherRegistrations.All()
		        join p in _persons.All() on tr.SSN equals p.SSN
                where tr.Type == TeacherType.MainTeacher
		        select new {tr, p};

		    var courses = (
                from cq in coursesLeftQuery
                join tq in teachersRightQuery on cq.c.ID equals tq.tr.CourseInstanceID into gj
                from teacher in gj.DefaultIfEmpty()
				select new CourseInstanceDTO
				{
					Name               = cq.ct.Name,
					TemplateID         = cq.c.CourseID,
					CourseInstanceID   = cq.c.ID,
					MainTeacher        = (teacher == null ? string.Empty : teacher.p.Name)
				}).ToList();

			return courses;
		}
	}
}
