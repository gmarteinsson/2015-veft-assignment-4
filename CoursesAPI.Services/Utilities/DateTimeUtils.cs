﻿namespace CoursesAPI.Services.Utilities
{
	public class DateTimeUtils
	{
		public static bool IsLeapYear(int year)
		{
		    var dividebyfour       = year % 4;
		    var dividebyhundred    = year % 100;
		    var dividebyfourundred = year % 400;

            if (dividebyfour != 0)
		    {
		        return false;
		    }
            else if (dividebyhundred != 0)
            {
                return true;
            }
		    else if (dividebyfourundred != 0)
		    {
		        return false;
		    }
			return true;
		}
	}
}
