# README #

This README is of School Project in Web Services from Reykjavik University.

### Contributors ###

Gunnar Marteinsson
Sigurður Már Atlason


### Assignment 4 - Unit Tests ###

In this assignment you will write unit tests and business logic, but you won't need to connect the given business logic to a Web API. I.e. you will only be using test data and running tests in the Test Explorer.

This assignment has two parts:

### (50%) Write business logic until given tests pass. ### 

The grade for this part will be calculated based on how many tests will pass.
There are two methods you need to write business logic for:
DateTimeUtils.IsLeapYear() - 5 tests (10%)

* This function should return true if a given year is a leap year, but false otherwise.
* Standard rules should be used: a year is a leap year if it is divisible by 4, unless it is divisible by 100 (except when it is divisible by 400).

 ### Write unit tests for given business logic. ###

Your task is to write unit tests (including the definition of test data) for the function GetCoursesBySemester(). Note that the provided project already defines some test data, but you should add more data as appropriate to ensure everything works as expected. Also note that this was mostly covered in lecture(s).

The tests should ensure that the following business logic rules are implemented:

* (10%) The function should return all courses on a given semester (no more, no less!)
* (10%) If no semester is defined, it should return all courses for the semester 20153
* (10%) For each course returned, the name of the main teacher of the course should be included (see the definition of CourseInstanceDTO).
* (10%) If the main teacher hasn't been defined, the name of the main teacher should be returned as an empty string.

Finally, the code is not correct when it comes to returning the name of the main teacher. You should:

1. Write the unit tests for this functionality (which should fail)
2. Modify the code until the tests pass.